cmake_minimum_required(VERSION 3.1.0)
project(demo)

# c++11 required
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include_directories(.)
include_directories(include)

add_executable(rundemo find_sum.cpp)
