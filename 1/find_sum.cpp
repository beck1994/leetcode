#include <map>
#include <iostream>

int main()
{
	int target = 9;
	int nums[10] = {1, 3, 5, 4, 1, 6, 7, 2, 9, 8};

	std::map<int, int> mynum;

	for(int i = 0; i < 10; i++)
	{
		if(mynum.find(target - nums[i]) != mynum.end())
		{
			std::cout<<nums[i]<<" "<<target-nums[i]<<std::endl;
		}

		mynum[nums[i]] = i;
	}
	return 0;
}
