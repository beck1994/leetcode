#include <map>
#include <iostream>

int main()
{
	int nums[10] = {1, 3, 1, 4, 1, 4, 7, 2, 4, 6};

	std::map<int, int> mynum;

	// 对元素超过3个的元素进行统计
	for(int i = 0; i < 10; i++)
	{
		if(mynum.find(nums[i]) != mynum.end())
		{
			mynum[nums[i]] += 1;
		}
		else
		{
			mynum[nums[i]] = 0;
			mynum[nums[i]] += 1;
		}
	}

	// 筛选出合适的元素
	for(auto it=mynum.begin(); it!=mynum.end(); it++)
	{
		if(it->second >= 3)
		{
			std::cout<<it->first<<std::endl;
		}
	}

	return 0;
}
